# Training

[Kubernetes](https://kubernetes.io/), [MicroK8s](https://microk8s.io/) and all other technologies being used in this project 
require some training to know what they are about and how to use them correctly. Below I'll list the trainings I've found to
get my knowledge at a (hopefully) sufficient level to make this project a success. 

## General

- [Introduction to Cloud Infrastructure Technologies (LFS151x)](https://training.linuxfoundation.org/training/introduction-to-cloud-infrastructure-technologies/)

## Kubernetes

- [Introduction to Kubernetes (LFS158x)](https://training.linuxfoundation.org/training/introduction-to-kubernetes/)

## MicroK8s

- [What is MicroK8s?](https://www.youtube.com/watch?v=v9KI2BAF5QU)
- [An intro to MicroK8s](https://www.brighttalk.com/webcast/6793/378029)

*To be continued ...*