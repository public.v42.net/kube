# Kube

This project aims to describe all steps to successfully setup and maintain a [`MicroK8s`](https://microk8s.io/) cluster
with all functionality required to use it as the basis for a broad range of applications. Be aware that [`Kubernetes`](https://kubernetes.io/),
[`MicroK8s`](https://microk8s.io/), [`Kafka` (Strimzi)](https://strimzi.io/), [`ScyllaDB`](https://www.scylladb.com/open-source-nosql-database/) 
and all other technologies being used in this project require some [training](TRAINING.md) to know what they are about
and how to use them correctly. [Read more](TRAINING.md) ...

***Note:*** This project has replaced the [`k3s`](https://k3s.io/) cluster as described in [`old/README.md`](old/README.md).
Several environments are still using `k3s` but will be replaced by `microk8s` in the near future.

## Hardware and OS

For initial tests I'm using Microsoft Hyper-V VM's on my main Windows 10 system, but for the 
*'real world'* scenarios I'm using VMware VM's being delivered by some more capable hardware.
In all cases I'm using the following virtual hardware as a starting point:
- 4 processors and 4096 MB of memory
- A 32 GB system disk and a 64 GB data disk

As `MicroK8s` is using [`snap`](https://snapcraft.io/docs/installing-snapd), all operating systems supporting 
[`snap`](https://snapcraft.io/docs/installing-snapd) can be used: I don't need to test them all separately.
When installing the OS, I'm using normal boot and swap partitions, but LVM for the *root* and *data* partitions:
- The normal boot and swap partitions on the 32 GB disk
- The *root* (`/`) partition as an LVM volume on the remainder of the 32 GB disk
- The *data* (`/var`) partition as an LVM volume on the 64 GB disk

Using LVM makes it easy to enlarge those partitions if more space is needed in the future. I've chosen Ubuntu
22 which includes [`snap`](https://snapcraft.io/docs/installing-snapd). For other OS-es, install [`snap`](https://snapcraft.io/docs/installing-snapd) as documented on their website.

***Note:*** I had some DNS issues when installing `MicroK8s` as offered by the installation of Ubuntu 22.
When installing `MicroK8s` as described below, those DNS issues were solved.

### Automatic updates

As user `kube` clone this repository:
```
cd /home/kube
git config --global credential.helper store
git clone https://gitlab.com/public.v42.net/kube.git
```

As user `root` copy [`/home/kube/kube/os/*.upg`](os) to 
`/root/kube/os/*.upg` with mode `0750`:
```
mkdir -p /root/kube
cp /home/kube/kube/*.upg /root/kube/
chmod 0750 /root/kube/*.upg
```
Now create a `root` crontab entry to run this script once a week at `01:01`:
```
1 1 * * <n> /root/kube/apt.upg >/root/kube/apt.upg.log 2>&1
```
Replace `<n>` by the node number, so node 1 will update itself on Monday,
node 2 on Tuesday, etcetera. Replace `apt.upg` by the correct alternative
if you are using another OS.

> When using more than 7 nodes, start again at day 1 (Monday) for node 8 ...

## MicroK8s

As I'm using Ubuntu 22, `MicroK8s` was an option during the OS installation, but this resulted in some DNS issues so in the 
end I installed `MicroK8s` manually as described here. Installing `MicroK8s` on all supported OS-es is as easy as following
the [Getting Started](https://microk8s.io/docs/getting-started) *Tutorial*. Once `MicroK8s` is up and running, follow the 
[Create a MicroK8s cluster](https://microk8s.io/docs/clustering) *How To* if you are setting up a multi-node cluster.

***Tip:*** use `snap alias microk8s.kubectl kubectl` to be able to use the `kubectl` command.

### DNS

By default the [`MicroK8s` dns addon](https://microk8s.io/docs/addon-dns) points to Google’s 8.8.8.8 and 8.8.4.4 servers 
for resolving addresses. To make sure that it uses our own DNS environment, enable the addon manually before it gets
enabled as a dependency for another addon with the following command:
```
microk8s enable dns:10.8.8.8,10.8.4.4
```
Replace the IP addresses by the addresses of your own DNS servers. If the addon was already enabled, the 
[`MicroK8s` dns addon](https://microk8s.io/docs/addon-dns) shows how it can be modified afterwards.

### Ingress

Use the command `microk8s enable ingress` as described in the [`MicroK8s` ingress addon](https://microk8s.io/docs/addon-dashboard)
documentation to enable an `nginx` ingress which by default uses a selfsigned fake certificate. To use an existing certificate,
you will need the certificate file `fullchain.pem` and the matching private key `privkey.pem`. Use the following command to create
or upgrade the default TLS certificate to be used by the ingress:
```
kubectl create secret tls default-tls --cert=fullchain.pem --key=privkey.pem --save-config --dry-run=client -o yaml | microk8s kubectl apply -f -
```
To tell the `nginx` ingress to use this TLS certificate, edit the `nginx-ingress-microk8s-controller` daemonset with the following command:
```
kubectl edit -n ingress daemonset.apps/nginx-ingress-microk8s-controller
```
Under `spec/template/spec/containers/args` add the option `--default-ssl-certificate=default/default-tls` and allow
the `nginx-ingress` daemonset to restart the `nginx-ingress` pods with the updated TLS certificate before checking the result.

### Dashboard

Use the command `microk8s enable dashboard` as described in the [`MicroK8s` dashboard addon](https://microk8s.io/docs/addon-dashboard) documentation to enable the [Kubernetes Dashboard](https://github.com/kubernetes/dashboard). The documentation als explains how the 
dashboard can be tested, but not how the dashboard can be configured for external access. The ingress definition to allow external 
access can be found in [`dashboard/ingress.yaml`](dashboard/ingress.yaml). To apply it use:
```
kubectl apply -f dashboard/ingress.yaml
```
Now you will be able to access the dashboard at `https://<kube-address>/dashboard/`. To generate a token use this command:
```
kubectl create token default
```
This token will have a limited lifetime. I still have to investigate how to configure different (user-based) login methods.

### Storage

The `MicroK8s` storage addon for `MayaStor` is still in development and the transport mechanism it uses is not fully 
supported in several OS flavors I'm using. When testing it I was unable to get it operational, so I decided to stick
to [`Longhorn`](https://longhorn.io/) which I have been testing before. 

*To be described ...*

## Kafka (Strimzi)

*To be described ...*

## ScyllaDB

*To be described ...*

