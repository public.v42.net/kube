#!/bin/bash
set -eu
if [ $# -ne 2 ] ; then
    echo "Usage: $0 <cert-file> <key-file>"
    exit 1
fi
crt=$(realpath $1)
key=$(realpath $2)
echo '-------------------------------------------------------------------------'
echo " Creating server-tls secret from $1 and $2 ..."
echo '-------------------------------------------------------------------------'
kubectl create secret generic server-tls --save-config --dry-run=client \
    --from-file=tls.crt=$crt --from-file=tls.key=$key --namespace default \
    -o yaml | kubectl apply -f -
echo '-------------------------------------------------------------------------'
echo ' DONE !!!'
echo '-------------------------------------------------------------------------'
