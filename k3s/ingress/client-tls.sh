#!/bin/bash
set -eu
KUBE_PATH=$(dirname $(realpath $0))
KUBE_ENVS=$(ls $KUBE_PATH/client-tls/*.pem|tr '\n' '\0'|xargs -0 -n 1 basename)
KUBE_ENVS=$(echo $KUBE_ENVS|sed -e 's/\.pem//g'|tr ' ' '|')
if [ $# -ne 1 ] ; then
    echo "Usage: $0 $KUBE_ENVS"
    exit 1
fi
echo '-------------------------------------------------------------------------'
echo " Creating client-tls secret from client-tls/$1.pem and user-tls/*.pem ..."
echo '-------------------------------------------------------------------------'
cat $KUBE_PATH/client-tls/$1.pem >$KUBE_PATH/client-tls.pem
cat $KUBE_PATH/user-tls/*.pem >>$KUBE_PATH/client-tls.pem
kubectl create secret generic client-tls --save-config --dry-run=client \
    --from-file=ca.crt=$KUBE_PATH/client-tls.pem --namespace default   \
    -o yaml | kubectl apply -f -
rm -f $KUBE_PATH/client-tls.pem
echo '-------------------------------------------------------------------------'
echo ' DONE !!!'
echo '-------------------------------------------------------------------------'
