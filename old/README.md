# Kube (old)

**NOTE:** This project has been replaced by a [`MicroK8s`](https://microk8s.io/) cluster as described in [`../README.md`](../README.md). 
Several environments are still using `k3s` but will be replaced by `microk8s` in the near future.

This project aims to describe all steps to successfully setup and maintain a [`k3s`](https://k3s.io/) cluster
with all functionality required to use it as the basis for a broad range of applications.
The description of the OS installation includes cloning this repository to the installed system.
In this way all helper scripts and files provided by this project will also be available for use during all subsequent steps.

## Training

[Kubernetes](https://kubernetes.io/), [K3s](https://k3s.io/), [Traefik](https://traefik.io/), [Longhorn](https://longhorn.io/) and more technologies being used in this project require some training to know what they are about and how to use them correctly. 
[Read more](TRAINING.md) ...

## Hardware

For the initial tests I used Microsoft Hyper-V VM's on my main Microsoft Windows 10 system as described in [`HARDWARE.md`](HARDWARE.md).
Whenever using additional (virtual) hardware, I will update [`HARDWARE.md`](HARDWARE.md) to describe the configurations I've been using.
[Read more](HARDWARE.md) ...

## OS

[Red Hat Enterprise Linux 8](RHEL8.md) is the reference implementation and has been used for all further tests.
The following operating systems also have been fully tested for use as the basis for this project:

- [Oracle Linux 8](OL8.md) is fully compaltible with the [RHEL8](RHEL8.md) reference inplementation

The following operating systems have been tested but currently have issues:

- [Red Hat Enterprise Linux 9](RHEL9.md) is not yet supported by `K3s` v1.24, to be re-tested when v1.25 is stable.
- [Oracle Linux 9](OL9.md) is not yet supported by `K3s` v1.24, to be re-tested when v1.25 is stable.
- [Red Hat Enterprise Linux 7](RHEL7.md) has an issue with the traefik pod (see https://forums.rancher.com/t/red-hat-7-not-supported-anymore/38537)
- [Debian 11 (bullseye)](DEBIAN11.md) had communication issues when test in a cluster with Red Hat and Oracle Linux 8
- [Oracle Linux 7](OL7.md) has an issue with the traefik pod (see https://forums.rancher.com/t/red-hat-7-not-supported-anymore/38537)

The following operating systems have to be re-tested:

- [Debian 9 (stretch)](DEBIAN9.md)
- [Debian 10 (buster)](DEBIAN10.md)

The following operating systems have been tested but did not meet the prerequisites to be able to run [`k3s`](https://k3s.io/):

- Red Hat Enterprise Linux 6 - *[ERROR]  Can not find systemd or openrc to use as a process supervisor for k3s*

This does *not* mean that *other* operating systems not mentioned here cannot be used: I just haven't tested them yet !!!

## K3s

[`K3S.md`](K3S.md) describes how to install, configure and manage [`K3s`](https://k3s.io/) on a supported OS as described above.
These are (still) manual tasks which are simplified by the helper scripts provided in the [`k3s`](k3s) folder:

- [`k3s/master`](k3s/master) to initialize a new `K3s` cluster, or to add another master node to an existing cluster
- [`k3s/agent`](k3s/agent) to add a `K3s` agent node to an existing cluster
- [`k3s/token`](k3s/token) to show the token of an existing cluster

[Read more](K3S.md) ...

## Traefik

`K3s` uses the [`Traefik`](https://traefik.io/) Ingress controller to route traffic between the external network and the running microservices. 
By default `Traefik` only uses port 80 for `http` traffic and port 443 for `https` traffic. 
We will configure the default SSL certificate, make the `Traefik` dashboard accessible and allow external `https` traffic to internal `http` endpoints, or to `https` endpoints with a self-signed certificate.
[Read more](TRAEFIK.md) ...

## Dashboard

The Kubernetes Dashboard is maintained at https://github.com/kubernetes/dashboard where the full documentation can
be found. To deploy the Kubernetes Web UI (Dashboard) [read more](DASHBOARD.md) ...

## Longhorn

*To be described ...*

