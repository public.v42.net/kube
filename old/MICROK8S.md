# MicroK8s

As an alternative to `K3s`, I recently started using `MicroK8s` and was impressed. To start using `MicroK8s` just follow the documentation and it works. On this page I'll list the additional steps I took, but most of them are straight from the `Kubernetes` documentation.

## Configuring a default TLS certificate

After enabling the `MicroK8s` ingress (it is using `nginx`), it uses a selfsigned fake certificate. To use an existing certificate, you will need the certificate file `fullchain.pem` and the matching private key `privkey.pem`. Use the following command to create or upgrade the default TLS certificate to be used by the ingress:
```
microk8s kubectl create secret tls default-tls --cert=fullchain.pem --key=privkey.pem --save-config --dry-run=client -o yaml | microk8s kubectl apply -f -
```
To tell `nginx` to use this TLS certificate, edit the `nginx-ingress-microk8s-controller` daemonset with the following command:
```
microk8s kubectl edit -n ingress daemonset.apps/nginx-ingress-microk8s-controller
```
Under `spec/template/spec/containers/args` add the option `--default-ssl-certificate=default/default-tls` and allow
the `nginx-ingress` daemonset to restart the `nginx-ingress` pods with the updated TLS certificate before checking the result.

## Enable external dashboard access 

After enabling the `MicroK8s` dashboard (the `Kubernetes` dashboard) the documentation only shows how to make the dashboard available 
with a proxy running in the foreground. To create an ingress definition for access to the dashboard, use `microk8s kubectl apply` 
to apply the following ingress definition:
```
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: kubernetes-dashboard
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /
spec:
  rules:
  - http:
      paths:
      - path: /dashboard
        pathType: Prefix
        backend:
          service:
            name: kubernetes-dashboard
            port:
              number: 443
```




