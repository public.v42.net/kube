# Dashboard

The Kubernetes Dashboard is maintained at https://github.com/kubernetes/dashboard where the full documentation can
be found. To deploy the Kubernetes Web UI (Dashboard) using the recommended configuration, use the following command:
```
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/master/aio/deploy/recommended.yaml
```
The output will confirm that all Dashboard components have been created correctly.
Give your cluster a few minutes to deploy the necessary pods. 
If you wish, you can check the status of the pods by running the command:
```
kubectl get pods -A
```
To check if the dashboard pod is operation, you can make it accessible with the following command:
```
kubectl port-forward -n kubernetes-dashboard --address 0.0.0.0 service/kubernetes-dashboard 8443:443
```
This will start a port forwarder (in the foreground) allowing access to the dashboard at 
[`https://<node-address>:8443/`](https://node-address:8443/) where you will be presented with a sign-in page.

## Authentication

To create and configure an `admin-user` service account, apply [`dashboard/admin-user.yaml`](dashboard/admin-user.yaml):
```
kubectl apply -f dashboard/admin-user.yaml
```
To generate an access token that can be used to sign-in, use the [`dashboard/admin-token`](dashboard/admin-token) script:
```
dashboard/admin-token
```
The generated token can be used to sign-in at [`https://<node-address>:8443/`](https://node-address:8443/).

## Ingress

Instead of keeping a foreground port forwarder running, an ingress route can be configured to make the dashboard available.
```
kubectl apply -f dashboard/ingress.yaml
```
The dashboard is now available at [`https://<node-address>/kubernetes-dashboard/`](https://node-address/kubernetes-dashboard/).
