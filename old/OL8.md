# Oracle Linux 8

>*Oracle Linux 8 is fully compatible with the [RHEL8](RHEL8.md) reference inplementation*

The [Oracle Linux 8](https://yum.oracle.com/oracle-linux-isos.html) OS installation 
and configuration as the base OS for the `kube` nodes is documented here.
- This procedure was tested with the `OL8U6 x86_64-boot.iso` DVD.

Boot from the [Oracle Linux 8](https://yum.oracle.com/oracle-linux-isos.html) DVD:
- Select `Install Oracle Linux 8.6.0` from the boot menu and wait for the installer to start
- Accept the selected language (I'm always using the default)
- Configure the `network & hostname`:
  - Enable the network (switch to `ON`) and configure the fully qualified hostname
  - To prevent possible issues I always disable IPv6 (under the `Configure...` button)
- Configure `date & time` for the correct timezone and enable NTP 
- Configure the `installation source` to use the Oracle repository URL:
  - https://yum.oracle.com/repo/OracleLinux/OL8/baseos/latest/x86_64/
- Configure `software selection` to `minimal install` with `standard` installation of Oracle Linux
- Configure the `installation destination`, , 
  - Select both disks and select custom storage configuration 
  - Click "Full disk summary" and check that `sda` is the boot disk
  - Click `Done` to start the custom storage configuration
  - If the disks were used before, remove all existing partitions
  - Create the default mount points automatically
  - Remove the `/home` partition
  - Modify the `/` partition set the type to `LVM` and the Volume Group `vg1` on disk `sda`
  - Create a `/var` partition of 64 GiB with type `LVM` and Volume Group `vg2` on disk `sdb`
  - Click `Done` and accept the changes you've made
- Configure the `root` password, preferably with the same `root` password on all `kube` nodes
- Create a new user named `kube`, preferably with the same password on all `kube` nodes (**NOT** the same as used for `root`)
- Click `begin installation` 

Wait until the installation completes and allow the system to reboot.

- Login as `root` and install all available updates:
  ```
  dnf -y update
  ```

## Additional packages

- Disable `firewalld` as it conflicts with the way `k3s` manages the networking rules
- Install `git` and `perl` as these are required to clone and use the helper scripts provided by this project
- Install `nfs-utils` and `iscsi-initiator-utils` as these are required by the `longhorn` storage provider
- Enable and start `iscsid`
```
systemctl disable firewalld --now
dnf -y install git perl
dnf -y install nfs-utils iscsi-initiator-utils 
systemctl enable iscsid
systemctl start iscsid
```

The minimal install does not install Network Manager. If it *has* been installed, it is required to disable nm-cloud-setup and reboot the node: 
```
systemctl disable nm-cloud-setup.service nm-cloud-setup.timer
reboot
```

## Configure an SSH key pair

For the `kube` nodes to be able to connect to each other using `ssh`, a key pair is needed for use by the `kube` user account. 
When you already have an SSH key pair on the *first* system, copy the `/home/kube/.ssh` directory from the *first* system to the *new* system, 
and check make sure the mode of all files is correct:
```
-rwx------ 1 kube kube /home/kube/.ssh
-rw------- 1 kube kube /home/kube/.ssh/authorized_keys
-rw------- 1 kube kube /home/kube/.ssh/id_ed25519
-rw------- 1 kube kube /home/kube/.ssh/id_ed25519.pub
```
When you do *not* yet have an SSH key pair (on the *first* node), as user `kube` generate a unique key pair with the following command:
```
$ ssh-keygen -t ed25519
```
Save the key at the *default* location and do *not* use a passphrase. The following files will be generated:
```
/home/kube/.ssh/id_ed25519
/home/kube/.ssh/id_ed25519.pub
```
Add the key to the `authorized_keys` file to allow remote `ssh` access with this key:
```
cat /home/kube/.ssh/id_ed25519.pub >>/home/kube/.ssh/authorized_keys
chmod 0600 /home/kube/.ssh/authorized_keys
```
Test the `ssh` connection between all `kube` nodes to make sure that everything functions correctly.

## Clone this repository

As user `kube` clone this repository:
```
cd /home/kube
git config --global credential.helper store
git clone https://gitlab.com/public.v42.net/kube.git
```

## Automatic updates

As user `root` copy [`/home/kube/kube/os/dnf.upg`](os/dnf.upg) to 
`/root/kube/os/dnf.upg` with mode `0750`:
```
mkdir -p /root/kube/os
cp /home/kube/kube/os/dnf.upg /root/kube/os/dnf.upg
chmod 0750 /root/kube/os/dnf.upg
```
Now create a `root` crontab entry to run this script once a week at `01:01`:
```
1 1 * * <n> /root/kube/os/dnf.upg >/root/kube/os/dnf.upg.log 2>&1
```
Replace `<n>` by the node number, so node 1 will update itself on Monday,
node 2 on Tuesday, etcetera.

> When using more than 7 nodes, start again at day 1 (Monday) for node 8 ...


