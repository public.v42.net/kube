# Debian 10 (buster)

>*Debian 10 (buster) has to be retested.
The procedure documented here has to be synchronized to the [RHEL8](RHEL8.md) reference inplementation*

The [Debian 10](https://www.debian.org/releases/buster/debian-installer/) (buster) OS installation 
and configuration as the base OS for the `kube` nodes is documented here.
- This procedure was tested with the `debian-10.12.0-amd64-netinst.iso` DVD.

Boot from the [Debian 10](https://www.debian.org/releases/buster/debian-installer/) DVD:
- Select `Graphical install` from the boot menu and wait for the installer to start
- Accept the selected language (I'm always using the default) and select your location 
- Accept the locale (I'm always using the default) and select the keyboard
- Configure the fully qualified hostname
- Configure the `root` password, preferably with the same `root` password on all `kube` nodes
- Create a new user named `kube`, preferably with the same password on all `kube` nodes (**NOT** the same as used for `root`)
- Select "guided - use entire disk" partitioning and select the 32 GB hard drive
- Select "all files in one partition" as the partitioning scheme 
- Check the result and allow the changes to be written to disk

Wait until the installation of the base system completes.
- Do not scan any additional media and configure the package manager for your location
- Select `SSH server` and `standard system utilities` to be installed, disable all other options

Wait until the installation of the selected software completes and allow the system to reboot.
- Login as `root` and install all available updates:
  ```
  apt -y update
  apt -y upgrade
  ```

## Additional packages

- Enabling `legacy iptables` as `nftables` conflicts with the way `k3s` manages the networking rules
- Install `curl`, `git`, `perl` and `sudo` as these are required to clone and use the helper scripts provided by this project
- Install `cgroup-tools` to make sure that `cgroups` is enabled which is required by `k3s`
- Install `nfs-utils` and `iscsi-initiator-utils` as these are required by the `longhorn` storage provider
- Enable and start `iscsid`
```
iptables -F
update-alternatives --set iptables /usr/sbin/iptables-legacy
update-alternatives --set ip6tables /usr/sbin/ip6tables-legacy
apt -y install curl git perl sudo
apt -y install cgroup-tools
apt -y install nfs-common open-iscsi 
reboot
```

## Configure an SSH key pair

For the `kube` nodes to be able to connect to each other using `ssh`, a key pair is needed for use by the `kube` user account. 
When you already have an SSH key pair on the *first* system, copy the `/home/kube/.ssh` directory from the *first* system to the *new* system, 
and check make sure the mode of all files is correct:
```
-rw------- 1 kube kube /home/kube/.ssh/authorized_keys
-rw------- 1 kube kube /home/kube/.ssh/id_ed25519
-rw-r--r-- 1 kube kube /home/kube/.ssh/id_ed25519.pub
```
When you do *not* yet have an SSH key pair (on the *first* node), as user `kube` generate a unique key pair with the following command:
```
$ ssh-keygen -t ed25519
```
Save the key at the *default* location and do *not* use a passphrase. The following files will be generated:
```
/home/kube/.ssh/id_ed25519
/home/kube/.ssh/id_ed25519.pub
```
Add the key to the `authorized_keys` file to allow remote `ssh` access with this key:
```
cat /home/kube/.ssh/id_ed25519.pub >>/home/kube/.ssh/authorized_keys
chmod 0600 /home/kube/.ssh/authorized_keys
```
Test the `ssh` connection between all `kube` nodes to make sure that everything functions correctly.

## Clone this repository

As user `kube` clone this repository:
```
cd /home/kube
git config --global credential.helper store
git clone https://gitlab.com/public.v42.net/kube.git
```

## Automatic updates

As user `root` copy [`/home/kube/kube/os/apt.upg`](os/apt.upg) to 
`/root/kube/os/apt.upg` with mode `0750`:
```
mkdir -p /root/kube/os
cp /home/kube/kube/os/apt.upg /root/kube/os/apt.upg
chmod 0750 /root/kube/os/apt.upg
```
Now create a `root` crontab entry to run this script once a week at `01:01`:
```
1 1 * * <n> /root/kube/os/apt.upg >/root/kube/os/apt.upg.log 2>&1
```
Replace `<n>` by the node number, so node 1 will update itself on Monday,
node 2 on Tuesday, etcetera.

> When using more than 7 nodes, start again at day 1 (Monday) for node 8 ...


