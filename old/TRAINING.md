# Training

[Kubernetes](https://kubernetes.io/), [K3s](https://k3s.io/), [Traefik](https://traefik.io/), [Longhorn](https://longhorn.io/) and more technologies being used in this project require some training to know what they are about and how to use them correctly. 
Below I'll list the trainings I've found to get my knowledge at a (hopefully) sufficient level to make this project a success. 

## General

- [Introduction to Cloud Infrastructure Technologies (LFS151x)](https://training.linuxfoundation.org/training/introduction-to-cloud-infrastructure-technologies/)

## Kubernetes

- [Introduction to Kubernetes (LFS158x)](https://training.linuxfoundation.org/training/introduction-to-kubernetes/)


## K3s

...

## Traefik

...

## Longhorn

...

