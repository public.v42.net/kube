# K3s

[`K3S.md`](K3S.md) describes how to install, configure and manage [`K3s`](https://k3s.io/) on a supported OS as described above.
These are (still) manual tasks which are simplified by the helper scripts provided in the [`k3s`](k3s) folder:

- [`k3s/master`](k3s/master) to initialize a new `K3s` cluster, or to add another master node to an existing cluster
- [`k3s/agent`](k3s/agent) to add a `K3s` agent node to an existing cluster
- [`k3s/token`](k3s/token) to show the token of an existing cluster

## Installing K3s

In the past an external `etcd` cluster was needed for a High Availability K3s cluster. But starting with release v1.19.5+k3s1 
[High Availability with an embedded DB](https://rancher.com/docs/k3s/latest/en/installation/ha-embedded/) is fully supported.

### Installing the *first* master node

To initialize the *first* master node of a new K3s cluster, use the [`k3s/master`](k3s/master) script with the `init` parameter. 

```
k3s/master init
```

The script will display the command used to install K3s before executing the command. Once completed, it will show the status
of the k3s cluster, as well as the token that is needed when adding master or worker nodes to the existing cluster.

### Adding another master node

To add another master node to an existing K3s cluster, use the [`k3s/master`](k3s/master) script with the parameters required 
to connect to the existing k3s cluster. The address should be the IP address of an existing master node:

```
k3s/master <address> <token>
```
The script will display the command used to install K3s before executing the command. Once completed, it will show the status
of the k3s cluster, as well as the token that is needed when adding more master nodes or agent nodes to the existing cluster.

### Adding an agent node

To add an agent node to an existing K3s cluster, use the [`k3s/agent`](k3s/agent) script with the parameters required 
to connect to the existing k3s cluster. The address should be the IP address of an existing master node. 

```
k3s/agent <address> <token>
```
  
If you don't have the token, you can run the [`k3s/token`](k3s/token) script on one of the existing master nodes.

## Configuring K3s

*To be documented ...*

## Managing K3s

*To be documented ...*
