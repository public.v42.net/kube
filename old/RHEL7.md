# Red Hat Enterprise Linux 7

>*Red Hat Enterprise Linux 7 has an issue with the traefik pod (see https://forums.rancher.com/t/red-hat-7-not-supported-anymore/38537).
The procedure documented here has to be synchronized to the [RHEL8](RHEL8.md) reference inplementation*

The [Red Hat Enterprise Linux 8](https://access.redhat.com/downloads) OS installation 
and configuration as the base OS for the `kube` nodes is documented here.
- This procedure was tested with the `rhel-server-7.9-x86_64-dvd.iso` DVD.

Note that you need to register to download and use the free versions of Red Hat Enterprise Linux.

Boot from the [Red Hat Enterprise Linux 7](https://access.redhat.com/downloads) DVD:
- Select `Install Red Hat Enterprise Linux 7.9` from the boot menu and wait for the installer to start
- Accept the selected language (I'm always using the default)
- Configure the `network & hostname`:
  - Enable the network (switch to `ON`) and configure the fully qualified hostname
  - To prevent possible issues I always disable IPv6 (under the `Configure...` button)
- Configure `date & time` for the correct timezone and enable NTP 
- Check the `installation source` and set `software selection` to minimal install
- Configure the `installation destination`, , 
  - Select both disks and select custom storage configuration 
  - Click "Full disk summary" and check that `sda` is the boot disk
  - Click `Done` to start the custom storage configuration
  - If the disks were used before, remove all existing partitions
  - Create the default mount points automatically
  - Remove the `/home` partition
  - Modify the `/` partition to only use `sda`, set the type to `LVM` and the Volume Group to `vg1` 
  - Create a `/var` partition of 64 GiB only using `sdb`, type `LVM` and Volume Group `vg2`
  - Click `Done` and accept the changes you've made
- Click `begin installation` 
- Configure the `root` password, preferably with the same `root` password on all `kube` nodes
- Create a new user named `kube`, preferably with the same password on all `kube` nodes (**NOT** the same as used for `root`)

Wait until the installation completes and allow the system to reboot.

- Login as `root`, register the system and install all available updates:
  ```
  subscription-manager register --username '<redhat-username>' --password '<redhat-password>' --auto-attach
  yum -y update
  ```

## Additional packages

- Disable `firewalld` as it conflicts with the way `k3s` manages the networking rules
- Install `git` and `perl` as these are required to clone and use the helper scripts provided by this project
- Install `nfs-utils` and `iscsi-initiator-utils` as these are required by the `longhorn` storage provider
- Enable and start `iscsid`
```
systemctl disable firewalld --now
yum -y install git perl
yum -y install nfs-utils iscsi-initiator-utils 
systemctl enable iscsid
systemctl start iscsid
```

The minimal install does not install Network Manager. If it *has* been installed, it is required to disable nm-cloud-setup and reboot the node: 
```
systemctl disable nm-cloud-setup.service nm-cloud-setup.timer
reboot
```

## Configure an SSH key pair

For the `kube` nodes to be able to connect to each other using `ssh`, a key pair is needed for use by the `kube` user account. 
When you already have an SSH key pair on the *first* system, copy the `/home/kube/.ssh` directory from the *first* system to the *new* system, 
and check make sure the mode of all files is correct:
```
-rwx------ 1 kube kube /home/kube/.ssh
-rw------- 1 kube kube /home/kube/.ssh/authorized_keys
-rw------- 1 kube kube /home/kube/.ssh/id_ed25519
-rw------- 1 kube kube /home/kube/.ssh/id_ed25519.pub
```
When you do *not* yet have an SSH key pair (on the *first* node), as user `kube` generate a unique key pair with the following command:
```
$ ssh-keygen -t ed25519
```
Save the key at the *default* location and do *not* use a passphrase. The following files will be generated:
```
/home/kube/.ssh/id_ed25519
/home/kube/.ssh/id_ed25519.pub
```
Add the key to the `authorized_keys` file to allow remote `ssh` access with this key:
```
cat /home/kube/.ssh/id_ed25519.pub >>/home/kube/.ssh/authorized_keys
chmod 0600 /home/kube/.ssh/authorized_keys
```
Test the `ssh` connection between all `kube` nodes to make sure that everything functions correctly.

## Clone this repository

As user `kube` clone this repository:
```
cd /home/kube
git config --global credential.helper store
git clone https://gitlab.com/public.v42.net/kube.git
```

## Automatic updates

As user `root` copy [`/home/kube/kube/os/yum.upg`](os/yum.upg) to 
`/root/kube/os/yum.upg` with mode `0750`:
```
mkdir -p /root/kube/os
cp /home/kube/kube/os/yum.upg /root/kube/os/yum.upg
chmod 0750 /root/kube/os/yum.upg
```
Now create a `root` crontab entry to run this script once a week at `01:01`:
```
1 1 * * <n> /root/kube/os/yum.upg >/root/kube/os/yum.upg.log 2>&1
```
Replace `<n>` by the node number, so node 1 will update itself on Monday,
node 2 on Tuesday, etcetera.

> When using more than 7 nodes, start again at day 1 (Monday) for node 8 ...


