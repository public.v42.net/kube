# Hardware

For the initial tests I used Microsoft Hyper-V VM's on my main Microsoft Windows 10 system.
Whenever using additional hardware, I will update this document to describe the hardware configurations I've been using.

## Microsoft Hyper-V 1

To test `K3s` compatibility, systems with 2 GB memory a 2 CPU's were sufficient:

- A Hyper-V generation 2 virtual machine
- 2048 MB RAM (*disable* dynamic memory) and 2 virtual processors
- 32 GB hard drive for the OS and `K3s`
- A mounted OS installation DVD
- Network adapter connected to the bridged switch
- Disable secure boot and disable checkpoints
- Firmware boot order with the DVD as first entry

## Microsoft Hyper-V 2

To test `K3s` with `Longhorn` at least 4 GB memory and 4 CPU's were required, as well as an additional data disk:

- A Hyper-V generation 2 virtual machine
- 4096 MB RAM (*disable* dynamic memory) and 4 virtual processors
- 32 GB hard drive for the OS
- 64 GB data drive for use by `k3s` and `Longhorn`
- A mounted OS installation DVD
- Network adapter connected to the bridged switch
- Disable secure boot and disable checkpoints
- Firmware boot order with the DVD as first entry

## VMware 

*To be described ...*

