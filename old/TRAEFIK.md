# Traefik

`K3s` uses the [`Traefik`](https://traefik.io/) Ingress controller to route traffic between the external network and the running microservices. 
By default `Traefik` only uses port 80 for `http` traffic and port 443 for `https` traffic. 
We will configure the default SSL certificate, make the `Traefik` dashboard accessible and allow external `https` traffic to internal `http` endpoints, or to `https` endpoints with a self-signed certificate.

- [Configure the default SSL certificate](#configure-the-default-ssl-certificate)
- [Configure Traefik dashboard access](#configure-traefik-dashboard-access)
- [Allow `https` to unsecure endpoints](#allow-https-to-unsecure-endpoints)

## Configure the default SSL certificate

To configure the external SSL certificate (full chain) and its corresponding private key (*not* password protected)
as a kubernetes secret, use the [`traefik/default-tls`](traefik/default-tls) helper script:
```
traefik/default-tls <certificate-file> <private-key-file>
```
This will create a `secret` object containing the SSL certificate and private key. Whenever the certificate needs to be renewed, use this helper script again to update the `secret` object with the new SSL certificate and private key.

To configure `Traefik` to use this `secret` object as the default SSL certificate, apply [`traefik/default-tls.yaml`](traefik/default-tls.yaml)
```
kubectl apply -f traefik/default-tls.yaml
```

## Configure Traefik dashboard access

To configure an IngressRoute for access to the `Traefik` dashboard, apply [`traefik/dashboard.yaml`](traefik/dashboard.yaml)
```
kubectl apply -f traefik/dashboard.yaml
```
The dashboard is now accessible at `https://<any-node>/dashboard/` (the trailing slash is mandatory).

## Allow `https` to `http` endpoints

*To be documented ...*

## Allow `https` to selfsigned endpoints

As our environment is completely self-managed and will not run any foreign services, using 
selfsigned certificates for microservices providing an internal `https` endpoint can be allowed with:
```
kubectl apply -f traefik/self-signed.yaml
```
If this is not acceptable in your environment, all microservices providing an internal `https` endpoint 
will need to be configured with valid SSL certificates which will need to be renewed once a year.
